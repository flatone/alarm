package jp.umeapp.alarm;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static android.support.v7.widget.RecyclerView.*;

public class EditListAdapter extends BaseAdapter{
    String[] alarmArray;
    Context mContext;
    LayoutInflater mLayoutInflater;

    public EditListAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
    }

    static class ViewHolder{
        public TextView tv_edit_time_list;
        public View bt_del;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = mLayoutInflater.inflate(R.layout.edit_list_layout, null);
            holder = new ViewHolder();
            holder.tv_edit_time_list = convertView.findViewById(R.id.tv_edit_time_list);
            holder.bt_del = convertView.findViewById(R.id.imageView3);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        //登録されているアラームを取得
        DatabaseEditor databaseEditor = new DatabaseEditor(mContext);
        alarmArray = databaseEditor.getAll();

        if(alarmArray[position] != null) {
            holder.tv_edit_time_list.setText(alarmArray[position]);
        }

        return convertView;
    }
}
