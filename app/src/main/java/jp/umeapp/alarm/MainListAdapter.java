package jp.umeapp.alarm;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

public class MainListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Map<String,Object>> timeAndIsEnableList;
    private Context mainContext;

    MainListAdapter(Context context,List<Map<String,Object>> timeList){
        inflater = LayoutInflater.from(context);
        timeAndIsEnableList = timeList;
        mainContext=context;
    }

    public View getView(final int position, View convertView, ViewGroup parent){
        convertView = inflater.inflate(R.layout.main_list_layout,parent,false);
        TextView textView = convertView.findViewById(R.id.tv_time_list);
        textView.setText(timeAndIsEnableList.get(position).get("time").toString());

        //有効か無効かで画像を変更
        ImageView imageView = convertView.findViewById(R.id.iv_alarm_is_enable);
        if((boolean)timeAndIsEnableList.get(position).get("isEnable")==true){
            imageView.setImageResource(R.drawable.ic_baseline_check_box_24px);
        }else{
            imageView.setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24px);
        }

        //画像をクリックされたら、MainActivityのchangeAlarmを実行
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = new MainActivity();
                Log.d("database",timeAndIsEnableList.get(position).get("time").toString());
                mainActivity.changeAlarm(mainContext,timeAndIsEnableList.get(position).get("time").toString()
                        ,(boolean)timeAndIsEnableList.get(position).get("isEnable"));
            }
        });

        return convertView;
    }


    //ListViewのタップを無効化
    @Override
    public boolean isEnabled(int position) {
        return false;
    }


    @Override
    public int getCount(){
        return timeAndIsEnableList.size();
    }

    @Override
    public Object getItem(int position){
        return timeAndIsEnableList.get(position);
    }

    @Override
    public long getItemId(int position){
        return 0;
    }




}

