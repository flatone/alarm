package jp.umeapp.alarm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseEditor {

    Context context;
    public DatabaseEditor(Context context) {
        this.context = context;
    }

    //Timeを追加
    //追加出来ればtrue　追加できなければfalse
    boolean add(String time) {
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        if (!checkExist(time)) {
            ContentValues newItem = new ContentValues();
            newItem.put(AlarmContract.Alarms.TIME, time);
            //アラーム有効にするようにisEnable trueは1
            newItem.put(AlarmContract.Alarms.IS_ENABLE,1);
            long newId = database.insert(
                    AlarmContract.Alarms.TABLE_NAME,
                    null,
                    newItem
            );
            database.close();
            return true;
        } else {
            database.close();
            return false;
        }
    }

    //削除
    void delete(String time) {
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        int deletedCount = database.delete(
                AlarmContract.Alarms.TABLE_NAME,
                AlarmContract.Alarms.TIME + " = ?",
                new String[]{time}
        );
        database.close();
    }

    //全ての時間(String)のデータを取得
    String[] getAll() {
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        Cursor cursor = null;
        cursor = database.query(
                AlarmContract.Alarms.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        String[] times = new String[cursor.getCount()];

        int i = 0;
        boolean next = cursor.moveToFirst();
        while (next) {
            times[i] = cursor.getString(cursor.getColumnIndex(AlarmContract.Alarms.TIME));
            i = i + 1;
            next = cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return times;
    }

    //同じ時刻が既に存在するか調べる
    //存在すればtrue 存在しなければfalse
    private boolean checkExist(String time) {
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        Cursor cursor = null;
        cursor = database.query(
                AlarmContract.Alarms.TABLE_NAME,
                null,
                AlarmContract.Alarms.TIME + " = ?",
                new String[]{time},
                null,
                null,
                null,
                null
        );
        int c = cursor.getCount();
        cursor.close();
        database.close();
        if (c == 0) {
            return false;
        } else {
            return true;
        }
    }


    //時刻のデータ(String)とそれが有効になってるかどうか(int)を取得し、Listを返す
    List<Map<String,Object>> getTimeDataAndIsEnable(){
        Log.d("database","get time and isEnable data");
        List<Map<String,Object>> timeDataAndIsEnableList = new ArrayList<>();
        Map<String,Object> dbTempMap;
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getReadableDatabase();

        Cursor cursor = database.query(AlarmContract.Alarms.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null);

        String dbTempString;
        int dbTempInt;
        boolean dbTempBoolean;

        //DBから取得
        if(cursor.moveToFirst()){
            Log.d("database","database have data");
            do{
                dbTempString=cursor.getString(cursor.getColumnIndex(AlarmContract.Alarms.TIME));
                dbTempInt=cursor.getInt(cursor.getColumnIndex(AlarmContract.Alarms.IS_ENABLE));

                //Int型なのをBool型に変えてリストに追加
                dbTempBoolean=intToBoolean(dbTempInt);

                dbTempMap = new HashMap<>();
                dbTempMap.put("time",dbTempString);
                dbTempMap.put("isEnable",dbTempBoolean);

                timeDataAndIsEnableList.add(dbTempMap);
            }while(cursor.moveToNext());

        }else{
            //DBに何も追加されてなかったとき
            Log.d("database","database is Empty");
        }

        cursor.close();
        database.close();
        return  timeDataAndIsEnableList;
    }


    //アラームを有効化
    void enableAlarm(String time){
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        ContentValues updateValues = new ContentValues();
        updateValues.put(AlarmContract.Alarms.IS_ENABLE,1);

        StringBuilder whereClause = new StringBuilder();
        whereClause.append(AlarmContract.Alarms.TIME);
        whereClause.append(" = ");
        whereClause.append("'");
        whereClause.append(time);
        whereClause.append("'");

        int update = database.update(AlarmContract.Alarms.TABLE_NAME,updateValues,whereClause.toString(),null);
        if(update==-1) Log.d("database","can't enable alarm");

        database.close();
    }


    //アラームを無効化
    void disableAlarm(String time){
        AlarmOpenHelper openHelper = new AlarmOpenHelper(context);
        SQLiteDatabase database = openHelper.getWritableDatabase();

        ContentValues updateValues = new ContentValues();
        updateValues.put(AlarmContract.Alarms.IS_ENABLE,0);

        StringBuilder whereClause = new StringBuilder();
        whereClause.append(AlarmContract.Alarms.TIME);
        whereClause.append(" = ");
        whereClause.append("'");
        whereClause.append(time);
        whereClause.append("'");

        int update = database.update(AlarmContract.Alarms.TABLE_NAME,updateValues,whereClause.toString(),null);
        if(update==-1) Log.d("database","can't disable alarm");

        database.close();
    }



    //IntをBooleanに変換
    private boolean intToBoolean(int temp) {
        if (temp == 1) return true;
        else return false;
    }

}
