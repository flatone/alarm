package jp.umeapp.alarm;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

public class EditActivity extends AppCompatActivity {

    private EditListAdapter mEditListAdapter;
    FloatingActionButton bt_goAdd;
    ListView editListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        final String packageName = getPackageName();

        //AddActivityへの画面遷移
        bt_goAdd = findViewById(R.id.bt_goAdd);
        bt_goAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName(packageName,packageName+".AddActivity");
                startActivity(intent);
            }
        });

        //ListViewへの各viewの貼り付け
        editListView = findViewById(R.id.editListView);
        mEditListAdapter = new EditListAdapter(this);
        editListView.setAdapter(mEditListAdapter);

    }
}
