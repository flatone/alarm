package jp.umeapp.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class MainActivity extends AppCompatActivity {

    private List<Map<String,Object>> timeAndIsEnableList;

    //Adapter登録用
    private static final String[] FROM ={"time"};
    private static final int[] To = {R.id.tv_time_list};

    //Toast表示用
    private static String disableMassage,enableMassage;

    private static ListView mainListview;
    private static AlarmManager alarmManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainListview = findViewById(R.id.lv_times_main);
        disableMassage = getText(R.string.disable_alarm).toString();
        enableMassage = getText(R.string.enable_alarm).toString();
        alarmManager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
    }


    @Override
    protected void onResume(){
        super.onResume();
        setListView(this);
        resetAndSetAlarm(this);
    }

    //リストを描画する
    private void setListView(Context context){
        DatabaseEditor databaseEditor = new DatabaseEditor(context);
        timeAndIsEnableList = databaseEditor.getTimeDataAndIsEnable();

        MainListAdapter adapter = new MainListAdapter(context,timeAndIsEnableList);
        mainListview.setAdapter(adapter);
    }


    //アラームをリセットしてからセット
    void resetAndSetAlarm(Context context){
        DatabaseEditor databaseEditor = new DatabaseEditor(context);
        timeAndIsEnableList = databaseEditor.getTimeDataAndIsEnable();

        for(int requestCode = 0; requestCode<timeAndIsEnableList.size();requestCode++){
            //一度すべてのアラームを解除
            Intent intentOld = new Intent(context,AlarmBroadcastReceiver.class);
            PendingIntent pendingIntentOld = PendingIntent.getBroadcast(context, requestCode, intentOld, 0);
            pendingIntentOld.cancel();
            alarmManager.cancel(pendingIntentOld);

            if((boolean)timeAndIsEnableList.get(requestCode).get("isEnable")==true){
                //アラームをセットし直す
                Intent intentNew = new Intent(context,AlarmBroadcastReceiver.class);
                intentNew.putExtra("time",timeAndIsEnableList.get(requestCode).get("time").toString());

                PendingIntent pendingIntentNew = PendingIntent.getBroadcast(context,requestCode,intentNew,0);

                int timeInt = timeStringToInt(timeAndIsEnableList.get(requestCode).get("time").toString());
                int timeHour = timeIntToHour(timeInt);
                int timeMinute = timeIntToMinute(timeInt);

                //時間をDBから取得してセット
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(System.currentTimeMillis());
                calendar.set(Calendar.HOUR_OF_DAY,timeHour);
                calendar.set(Calendar.MINUTE,timeMinute);
                calendar.set(Calendar.SECOND,0);
                calendar.set(Calendar.MILLISECOND,0);

                //セットした時間が、今日の現在時刻より前の場合は日にちを明日に
                if(calendar.getTimeInMillis()<System.currentTimeMillis()){
                    calendar.add(Calendar.DAY_OF_YEAR,1);
                }
                Log.d("time","requestCode:"+requestCode+" is "+timeHour+":"+timeMinute);
                //APIレベルごとに分岐
                if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                    alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), null), pendingIntentNew);
                }else if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT){
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentNew);
                }else{
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntentNew);
                }

            }
        }
    }


    //アラームが切り替えられたとき、データベースを更新して、リストも更新し、アラームをセットし直す
    void changeAlarm(Context context, String time,boolean isEnable){
        DatabaseEditor databaseEditor = new DatabaseEditor(context);
        //有効⇔無効
        if(isEnable){
            databaseEditor.disableAlarm(time);
            //Toastを出すか出さないか
            Toast.makeText(context, time + disableMassage, Toast.LENGTH_SHORT).show();
        }else{
            databaseEditor.enableAlarm(time);
            Toast.makeText(context, time + enableMassage, Toast.LENGTH_SHORT).show();
        }

        setListView(context);
        resetAndSetAlarm(context);
    }


    //DBのTimeがStringなのでIntに変換
    private int timeStringToInt(String time){
        time = time.replace(":","");
        int result = Integer.parseInt(time);
        return result;
    }

    //Timeから時を取得
    private int timeIntToHour(int time){
        int result = time/100;
        return result;
    }

    //Timeから分を取得
    private int timeIntToMinute(int time){
        int result = time%100;
        return result;
    }

    //編集ボタンを押したときEditActivity起動
    public void onClickDoEdit(View view){
        Intent intent = new Intent(this,EditActivity.class);
        startActivity(intent);
    }

}
