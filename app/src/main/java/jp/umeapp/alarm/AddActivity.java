package jp.umeapp.alarm;

import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class AddActivity extends AppCompatActivity {


    TextView textView;
    String time = "00:00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        textView = findViewById(R.id.tv_add_time);
    }

    //時刻タップ時
    public void tapText(View view) {
        final Calendar calendar = Calendar.getInstance();
        final int hour = calendar.get(Calendar.HOUR_OF_DAY);
        final int minute = calendar.get(Calendar.MINUTE);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        time = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);
                        textView.setText(time);
                    }
                }, hour, minute, true);
        timePickerDialog.show();
    }

    //追加タップ時
    public void add(View view) {
        DatabaseEditor databaseEditor = new DatabaseEditor(this);
        if (databaseEditor.add(time)) {
            MainActivity mainActivity = new MainActivity();
            mainActivity.resetAndSetAlarm(this);
            finish();
        } else {
            Toast.makeText(this, "既に存在します", Toast.LENGTH_SHORT).show();
        }
    }
}
