package jp.umeapp.alarm;

import android.provider.BaseColumns;

public class AlarmContract {

    public AlarmContract() {}

    public static abstract class Alarms implements BaseColumns { // _id
        public static final String TABLE_NAME = "alarms";
        public static final String TIME = "time";
        public static final String IS_ENABLE = "isEnable";
    }
}
