package jp.umeapp.alarm;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class GameActivity extends AppCompatActivity {

    private int sum;//乱数を足し合わせた数
    Ringtone ringtone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        question();

        //画面が消えてときに画面点灯
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //アラーム音再生実装予定
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        ringtone = RingtoneManager.getRingtone(this, uri);

        ringtone.play();
    }

    //問題を生成
    private void question(){
        TextView textView = findViewById(R.id.tv_problem);
        int a = randomNumber();
        int b = randomNumber();

        textView.setText(a + "+" + b + "=");
        sum = a + b;
    }

    //0～9の乱数を生成
    private int randomNumber(){
        return (int)(Math.random() * 10);
    }

    //sumとユーザーの答えが等しいか検証
    private boolean calculationResult(){
        EditText editText = findViewById(R.id.editText);
        String s = editText.getText().toString();
        int answer = -1;

        if (!s.equals("")){
            answer = Integer.parseInt(s);
        }
        
        if (sum == answer){
            return true;
        }else {
            return false;
        }
    }

    //解除ボタン
    public void answer(View view){
        if (calculationResult()){
            //アラーム停止
            ringtone.stop();
            finish();
        }else {
            Toast.makeText(this,"正しくありません",Toast.LENGTH_SHORT).show();
        }
    }
}
