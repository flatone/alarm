package jp.umeapp.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmBroadcastReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {

        String time = intent.getStringExtra("time");
        DatabaseEditor databaseEditor = new DatabaseEditor(context);

        //アラームが起動したら、アラームの設定を無効にする
        databaseEditor.disableAlarm(time);

        //Gameアクティビティを起動
        Intent openGame = new Intent(context,GameActivity.class);
        openGame.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(openGame);
    }


}
