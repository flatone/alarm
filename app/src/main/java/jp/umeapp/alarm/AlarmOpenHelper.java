package jp.umeapp.alarm;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlarmOpenHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "alarm.db";
    public static final int DB_VERSION = 1;

    public static final String CREATE_TABLE =
            "create table " + AlarmContract.Alarms.TABLE_NAME + "(" +
                    AlarmContract.Alarms._ID + " integer primary key autoincrement, " +
                    AlarmContract.Alarms.TIME + " text, " + AlarmContract.Alarms.IS_ENABLE + " integer)";

    public static final String DROP_TABLE = "drop table if exists " + AlarmContract.Alarms.TABLE_NAME;

    public AlarmOpenHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }
}
